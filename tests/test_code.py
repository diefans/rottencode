def test_scanner_registry(mocker):
    import collections
    from rottencode import code

    mocker.patch.object(
        code.ScannerRegistry,
        "handlers",
        collections.defaultdict(lambda: collections.defaultdict(set)),
    )

    class Foo:
        ...

    class Bar(Foo):
        ...

    @code.Scanner.register(Foo)
    def handle_foo(node):
        ...

    s = code.Scanner()

    @s.register(Bar)
    def handle_bar(node):
        ...

    assert set(code.ScannerRegistry.find(s, Foo)) == {handle_foo, handle_bar}
    assert set(code.ScannerRegistry.find(s, Bar)) == {handle_bar}
