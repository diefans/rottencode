import pytest


@pytest.fixture
def python(mocker):
    from rottencode import python

    return python


@pytest.mark.parametrize(
    "name, str_name", [("a.b.c", "a.b.c"), (("a", "b", "c"), "a.b.c")]
)
def test_dotted_name(python, name, str_name):

    assert str(python.Name(name)) == str_name


def test_dotted_name_parent(python):
    dn = python.Name("a.b.c")

    assert dn.parent == python.Name("a.b")


@pytest.mark.parametrize(
    "name, str_name", [("a.b.c", "foo.bar.a.b.c"), (("a", "b", "c"), "foo.bar.a.b.c")]
)
def test_dotted_name_join(python, name, str_name):
    dn = python.Name("foo.bar")
    dn = dn / name
    assert str(dn) == str_name


def test_module_from_dotted_name(python):
    m = python.Module(python.Name(__name__))
    assert str(m) == "tests.test_python"


def test_module_is_package(python):
    m = python.Module(python.Name("foo"), "foo/__init__.py")
    assert m.is_package


@pytest.mark.parametrize(
    "base, path, level, module, relative",
    [
        ("a.b.c.d", "d.py", 2, "x.y", "a.b.x.y"),
        ("a.b.c.d", "__init__.py", 2, "x.y", "a.b.c.x.y"),
    ],
)
def test_module_relate(python, base, path, level, module, relative):
    m = python.Module(base, path)
    assert relative == str(m.relate(level, module))
